import cv2
import numpy as np
from alive_progress import alive_bar

size = 16384

img = np.zeros((size, size, 3), np.uint8)

def coords(i, j):
    return (3 * i / size - 1.5, 3 * j / size - 2)
with alive_bar(size * size) as bar:
    for i in range(size):
        for j in range(size):
            y, x = coords(i, j)
            r, c = (0, 0)
            n = 0
            for n in range(100):
                nr = r * r - c * c + x
                c = 2 * r * c + y
                r = nr
                if r > 2 or r < -2 or c > 2 or c < -2:
                    break
            if n == 99:
                img[i][j] = (0,0,0)
            else:
                img[i][j] = (n * n + 50, n, 2 * n)
            bar()

cv2.imshow('Mandelbrot', img)
cv2.imwrite('results/mandelbrot-' + str(size) + '.png', img)
cv2.waitKey(0)
cv2.destroyAllWindows()
