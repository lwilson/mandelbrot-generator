#include <stdio.h>

int main() {
  int sx = 1024;
  int sy = 1024;
  float dx = 3.0 / sx;
  float dy = 3.0 / sy;
  // http://rosettacode.org/wiki/Bitmap/Write_a_PPM_file#C
  FILE *fp = fopen("result.ppm", "wb");
  (void) fprintf(fp, "P6\n%d %d\n255\n", sx, sy);
  for (float y = -1.5; y <= 1.5; y += dy) {
    for (float x = -2; x <= 1; x += dx) {
      unsigned char n = 0;
      float r = 0;
      float c = 0;
      for (;n < 255 && c < 2 && c > -2 && r < 2 && r > -2; n++) {
        float t = r * r - c * c + x;
        c = 2 * r * c + y;
        r = t;
      }
      static unsigned char rgb[3];
      rgb[0] = n * 2;
      rgb[1] = n;
      rgb[2] = n * n + 50;
      (void) fwrite(rgb, 1, 3, fp);
    }
  }
  (void) fclose(fp);
}
