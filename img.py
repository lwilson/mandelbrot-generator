import cv2
import numpy as np
from alive_progress import alive_bar

sx = 16384
sy = 16384

img = np.zeros((sx, sy, 3), np.uint8)

f = open("numbers.csv", "r")
ls = f.readlines()
f.close()

x = 0
y = 0

with alive_bar(sx * sy) as bar:
    for l in ls:
        d = l.split(',')
        for c in d:
            if c == ',' or c == '' or c == '\n' or x >= sx or y >= sy:
                continue
            n = int(c)
            img[y][x] = (n * n + 50, n, 2 * n)
            x += 1
            bar()
        x = 0
        y += 1

cv2.imshow('Mandelbrot', img)
cv2.imwrite('results/mandelbrot-' + str(sx) + "x" + str(sy) + '.png', img)
cv2.waitKey(0)
cv2.destroyAllWindows()
