#include <stdio.h>

int main() {
  float dx = 3.0 / 16384.0;
  float dy = 3.0 / 16384.0;
  for (float y = -1.5; y <= 1.5; y += dy) {
    for (float x = -2; x <= 1; x += dx) {
      unsigned char n = 0;
      float r = 0;
      float c = 0;
      for (;n < 255 && c < 2 && c > -2 && r < 2 && r > -2; n++) {
        float t = r * r - c * c + x;
        c = 2 * r * c + y;
        r = t;
      }
      printf("%d,", n);
    }
    printf("\n");
  }
}
